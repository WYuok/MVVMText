﻿using PrismText.Views;
using System.Configuration;
using System.Data;
using System.Windows;

namespace PrismText
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : PrismApplication
    {
        //主窗体注册在这 
        protected override Window CreateShell()
        {
            //return Container.Resolve<MainWindow>();
            //区域练习
            return Container.Resolve<RegionTest>();
        }
        //需要注册的对象，在这个方法里面注册
        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            //注册弹窗内容
            containerRegistry.RegisterDialog<Uecm>();
            //注册自定义弹窗,可以不注册使用框架默认弹窗
            containerRegistry.RegisterDialogWindow<SupWindow>();
            //注册导航画面到
            containerRegistry.RegisterForNavigation<ViewA>();
            containerRegistry.RegisterForNavigation<ViewB>();
        }
    }

}
