﻿using Prism.Ioc;
using PrismText.Modles;
using System.Windows;


namespace PrismText.Views
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow(IEventAggregator aggregator)
        {
            InitializeComponent();
            #region 消息对象练习
            //通过注入获取消息对象
            //通过消息对象进行订阅或者发表
            //事件对象
            //
            //订阅无参
            //aggregator.GetEvent<EvenMassage>().Subscribe(Receive);
            //订阅有参；可封装成一个对象实现带多个参数
            aggregator.GetEvent<EvenMassageAgs>().Subscribe(Receive);
            ////订阅有参，带过滤功能
            //aggregator.GetEvent<EvenMassageAgs>().Subscribe(Receive, obj => obj.Id == 1);
            ////订阅有参，设置线程
            //aggregator.GetEvent<EvenMassageAgs>().Subscribe(Receive, ThreadOption.PublisherThread);

            ////无参数发布
            //aggregator.GetEvent<EvenMassage>().Publish();
            ////有参数发布
            ////全部接收
            ////aggregator.GetEvent<EvenMassageAgs>().Publish("初学Prism框架");

            ////过滤触发这个条
            //aggregator.GetEvent<EvenMassageAgs>().Publish(new DataModle() { Id = 1, Content = "初学Prism框架" });
            #endregion
            //通过注入IContainerProvider拿到容器对象注册MainWindowViewModle

            //var vm = container.Resolve<MainWindowViewModle>();
            //this.DataContext = vm;

        }

        //无参触发
        private void Receive()
        {

        }
        //有参触发
        private void Receive(DataModle obj)
        {
            //通过事件总线方式与弹窗的交换
            var frm = new SupWindow();
            frm.ShowDialog();
        }
    }
}
