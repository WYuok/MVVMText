﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PrismText.Views
{
    /// <summary>
    /// RegionTest.xaml 的交互逻辑
    /// </summary>
    public partial class RegionTest : Window
    {
        private IRegionManager _regionManager;
        public RegionTest(IRegionManager regionManager)
        {
            InitializeComponent();
            //注入IRegionManager
            _regionManager = regionManager;
            //注册导航页面
            regionManager.RegisterViewWithRegion("ViewRegion", "ViewA");
            regionManager.RegisterViewWithRegion("ViewRegion", "ViewB");
            //找到导航里面区域，找到对应的导航页面并激活他
            this.Loaded += (e, ev) =>
            {
                var region = regionManager.Regions["ViewRegion"];
                var view = region.Views.First(v => v.GetType().Name == "ViewB");
                region.Activate(view);
            };
            
        }
    }
}
