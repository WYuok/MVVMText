﻿using Prism.Dialogs;
using PrismText.Modles;
using PrismText.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrismText.ViewModels
{
    public class MainWindowViewModel
    {
        private IDialogService _dialogService;
        private IEventAggregator _aggregator;
        public MainWindowViewModel(IEventAggregator aggregator,IDialogService dialog)
        {
            this._aggregator = aggregator;
            _dialogService = dialog;
            Opencommand = new DelegateCommand(OnOpen);
        }

        
        public DelegateCommand Opencommand { get; set; }
        public string str { get; set; } = "你好";

        private void OnOpen()
        {
            //通过事件总线打开弹窗
            //_aggregator.GetEvent<EvenMassageAgs>().Publish(new DataModle());

            //传入弹窗中的数据
            IDialogParameters parametrs = new DialogParameters();
            parametrs.Add("main", str);
            //通过IDialogService打开弹窗，将数据拿到弹窗执行并拿到弹窗返回的内容
            _dialogService.ShowDialog("Uecm", parametrs, new Action<IDialogResult>(OnClose));
        }
        //弹窗返回结果执行的业务
        private void OnClose(IDialogResult dialog) 
        {
        
        }
    }
}
