﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrismText.ViewModels
{
    public class UecmViewModel : IDialogAware
    {
        public UecmViewModel()
        {
            OnCloesCommand = new DelegateCommand(OnDialogClosed);
        }

        public DialogCloseListener RequestClose { get; set; }

        public bool CanCloseDialog()
        {
            return true;
        }

        public void OnDialogClosed()
        {
            DialogResult result = new DialogResult();
            result.Parameters.Add("A", true);
            RequestClose.Invoke(result);
        }

        public void OnDialogOpened(IDialogParameters parameters)
        {

        }
        public DelegateCommand OnCloesCommand { get; set; }
        //private void OnCloes()
        //{

        //}
    }
}
