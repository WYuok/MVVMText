﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using GalaSoft.MvvmLight.Threading;
using MVVMLightTest.Base;
using MVVMLightTest.Views;
namespace MVVMLightTest.ViewModels
{
    public class MainViewModel : ViewModelBase//ObservableObject
    {
        public MainViewModel()
        {
            //Value = "初学MVVM";
            Btncommand = new RelayCommand(btnCilck, canExecute);
            ValueList = new ObservableCollection<string>();
            
                Task.Run(async () =>
                {
                    await Task.Delay(2000);
                    try
                    {
                        //在UI线程执行
                        DispatcherHelper.CheckBeginInvokeOnUI(() =>
                        {
                            ValueList.Add("123");
                        });
                    }
                    catch (Exception ex)
                    {

                       
                    }

                });


        }
        public ObservableCollection<string> ValueList { get; set; }

        private string? _value = "你好";

        public string Value
        {
            get { return _value; }
            set
            {
                Set<string>(ref _value, value);
                Btncommand.RaiseCanExecuteChanged();
            }
        }

        
    


        //命令属性
        public RelayCommand? Btncommand { get; set; }
        private void btnCilck()
        {
            //发布
            //Messenger.Default.Send<string>(Value);
            Messenger.Default.Send<MessgeObj>(new MessgeObj()
            {
                Value = "数据",
                Action = new Action<bool>(result =>
                {


                })
            });

        }
        private bool canExecute()
        {
            return this.Value == "你好";
        }

        public RelayCommand TskCommand { get; set; }

            



    }
}
