﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVVMLightTest.Base
{
    public class MessgeObj
    {
        public string ?Value { get; set; }
        public Action<bool> ?Action { get; set; }
    }
}
