﻿
using GalaSoft.MvvmLight.Messaging;
using System.Windows;
using MVVMLightTest.Base;

namespace MVVMLightTest.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            //注册
            Messenger.Default.Register<string>(this, ExecuteMessage);
            Messenger.Default.Register<MessgeObj>(this, ExecuteMessage1);
        }

        private void ExecuteMessage(string obj)
        {
            
        }

        private void ExecuteMessage1(MessgeObj obj)
        {
            var result = new PopupWindow() { Owner =this}.ShowDialog()==true;
            obj?.Action(result);
        }
    }
}